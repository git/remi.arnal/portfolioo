export interface Item {
	type: string;
	imagePath: string;
	description: string;
	tags: string[];
	markdownPath: string;
}
