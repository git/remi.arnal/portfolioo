import { Component } from '@angular/core';
import { ProjectCardComponent } from '../project-card/project-card.component';
import { Item } from '../../../models/item.model'; 
import { ItemsService } from '../items.service'; 
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-internships',
  standalone: true,
  imports: [ProjectCardComponent, NgFor],
  templateUrl: './internships.component.html',
  styleUrl: './internships.component.css'
})
export class InternshipsComponent {
  items: Item[] = [];
  
  constructor(private itemService: ItemsService) {
    this.itemService.getItems().subscribe(
      (items) => {
        this.items = items.filter((item: Item) => item.type === 'internship');
      }
    )
  }
}
