import { Component, Input} from '@angular/core';
import { MarkdownService } from '../services/markdown.service'; 
import { NgIf } from '@angular/common';
import { MarkdownPipe } from '../markdown.pipe';

@Component({
  selector: 'app-markdown',
  standalone: true,
  imports: [NgIf, MarkdownPipe],
  providers: [ MarkdownService],
  templateUrl: './markdown.component.html',
  styleUrl: './markdown.component.css'
})
export class MarkdownComponent {
  @Input() file: string = '';
  md: string = '';

  markdownRoot = './assets/';
  extension = '.md';

  constructor(private markdownService: MarkdownService) {}

  ngOnInit() : void {
    this.markdownService.convertFromFile(this.markdownRoot+this.file+this.extension).subscribe(
      (markdown) => this.md = markdown
    );
  }
}
