import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MarkdownService {
  markdownContent : string | undefined = '';
  
  constructor(private http: HttpClient) {}

  convertFromFile(filepath: string): Observable<string> {
    return this.http.get(filepath, { responseType: 'text' });
  }
}
