import { NgFor } from '@angular/common';
import { Component,Input } from '@angular/core';
import { Item } from '../../../models/item.model';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-project-card',
  standalone: true,
  imports: [NgFor],
  templateUrl: './project-card.component.html',
  styleUrl: './project-card.component.css'
})
export class ProjectCardComponent {
  @Input() item!: Item;

  constructor(private router: Router) {}

  goToMarkdown(path: string) {
    this.router.navigate(['/md', path]);
  }
}
