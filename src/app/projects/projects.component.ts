import { Component } from '@angular/core';
import { ProjectCardComponent } from '../project-card/project-card.component';
import { ItemsService } from '../items.service';
import { Item } from '../../../models/item.model'; 
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [ProjectCardComponent, NgFor],
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.css'
})
export class ProjectsComponent {
  items: Item[] = [];
  
  constructor(private itemService: ItemsService) {
    this.itemService.getItems().subscribe(
      (items) => {
        this.items = items.filter((item: Item) => item.type === 'project');
      }
    )
  }
}
