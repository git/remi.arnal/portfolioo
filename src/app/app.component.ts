import { Component } from '@angular/core';
import { RouterOutlet, RouterLink } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { ProjectsComponent } from './projects/projects.component';
import { InternshipsComponent } from './internships/internships.component';
import { AboutComponent } from './about/about.component';
import { Router } from '@angular/router';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, NavbarComponent, ProjectsComponent, InternshipsComponent, AboutComponent, NgIf],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'portfolioo';

  constructor(private router: Router) { }

  isHomeRoute() {
    console.log("ROUTE", this.router.url)
    return this.router.url.split('#')[0] == "/";
  }
}
