import { Routes } from '@angular/router';
import { MarkdownComponent } from './markdown/markdown.component';

export const routes: Routes = [
  { path: 'md/:file', component: MarkdownComponent }
];
