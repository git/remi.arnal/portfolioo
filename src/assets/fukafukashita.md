# Fukafukashita


Fukafukashita est un projet réalisé en groupe de 4 pour apprendre à utiliser
Symphony. Il se veut être un forum facile d'utilisation et moderne pour
partager ses rêves et ses cauchemard et lire ceux des autres.

[*lien vers le code source*](https://codefirst.iut.uca.fr/git/Assassymfony)

## Fonctionalités

- Poster vos rêves et vos cauchemard
- Consulter les rêves sans connexion
- Rechercher un post par son contenu
- Abonnements
- Design spécial **Cauchemard** !

## Galerie

<img src="assets/fukafukashita_1.jpg">
<img src="assets/fukafukashita_2.png">
<img src="assets/fukafukashita_3.png">
<img src="assets/fukafukashita_4.png">

