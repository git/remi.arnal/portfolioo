# Blue Whale

J'ai effectué mon stage de 2ème année dans l'entreprise [Blue Whale](https://www.blue-whale.com)
ou j'ai travaillait sur l'ERP qui était en fin de service. Le nouvel ERP n'était pas encore fini,
j'ai donc dû aider à la transition entre les ERP au niveau de la base donnée mais aussi du code
de l'ancien ERP (Power Builder).

Ce fut un stage très formateur, j'ai dès le début eu des responsabilités au niveau du recueil de besoin,
résolution du bug et ajouts de nouvelles fonctionnalités. Ma mission la plus importante a été l'automatisation
de processus métier lourd et peu productif, les assistantes commerciales remplissaient des fichiers excel manuellement.
J'ai dirigé ce projet et recueilli les besoins auprès des assistantes commerciales (C5), qui a permis un grand gain de temps.
