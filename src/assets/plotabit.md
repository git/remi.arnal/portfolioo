# Plotabit

Plotabit est un projet d'IA effectué par groupe de 2 durant notre 3ème année.
Le but était d'apprendre la prédiction par IA en choisissant un jeu de donnée
puis en utilisant différent modèle pour classifier avec la meilleure précision.

Nous avons choisis un jeux de données contenant des observations de spectres lumineux
et astres associés: **QUASAR**, **ETOILES** et **GALAXIE**.

Pour analyser les données et optimiser l'entrainement de nos modèles nous avons utilisé
des histogrammes, boîtes à moustaches, matrice de corrélation et scatter plot. Cela nous
à permis de limiter le nombre de colonnes utilisées pour l'entrainement, le rendant plus court.

Nous avons aussi lancé un grand nombre d'entrainement avec différent paramètres pour trouver la
meilleure précision et mieux comprendre l'implication de ceux-ci dans les résulats.

[*lien vers le code source*](https://codefirst.iut.uca.fr/git/PyPloteam/Plotabit)

## Modèles

- K Nearest Neighbors
- Random Forest Classifier
- Decision Tree Classifier
- Linear SVC
- SGD
- Nearest Centroid
- MLP Classifier (neural network)

## Galerie

<img src="assets/plotabit_1.jpg">
<img src="assets/plotabit_2.jpg">
<img src="assets/plotabit_3.jpg">
<img src="assets/plotabit_4.jpg">
<img src="assets/plotabit_5.jpg">
