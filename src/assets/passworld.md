# Passworld

Passworld est un gestionnaire de mots de passe cross-plateforme développé
par un groupe de 5 personnes au cours de notre projet de 2ème année.

Le principe même du gestionnaire de mots de passe et de stocker vos mots de passes
à l'abri en toute sécurité. C'est ce que nous avons fait en utilisant AES-256 et
les keystore de chaque plateformes pour être protégé au mieux sur chacune d'elles.

La sauvegarde de vos informations est faite en arrière plan sur nos serveurs grâce
à une base de donnée et un Web-API à chacune des modifications.

[*lien vers le code source*](https://codefirst.iut.uca.fr/git/PassWorld)

## Fonctionalités

- Sauvegarde de mots de passe avec un mot de passe maître
- Synchronisation et récupération des données
- Authentification avec yubikey et empreinte digitale
- Générateur de mots de passe
- Analyse de la sécurité de vos mots de passe


## Galerie
<div class="gal-3">
  <img src="assets/passworld_1.jpg">
  <img src="assets/passworld_2.jpg">
  <img src="assets/passworld_3.jpg">
</div>

<div class="gal-3">
  <img src="assets/passworld_4.jpg">
  <img src="assets/passworld_5.jpg">
  <img src="assets/passworld_6.jpg">
</div>
