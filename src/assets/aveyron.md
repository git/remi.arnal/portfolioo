# DSI - Département de l'Aveyron

J'effectue actuellement mon stage 3ème année d'une durée de 16 semaines à la
Direction des systèmes d'Information du Département de l'Aveyron. Ma mission
durant ce stage et de faire évoluer l'intranet du département utilisé quotidiennement
par plus de 1500 agents.

Cet intranet livré par un prestataire manque de fonctionnalités et contient un grand
nombre de bugs et d'oublis. Pour rajouter les fonctionnalités il me faut relire le code
sans documentation et m'adapter à l'architecture de l'application.

Une de mes missions pendant ce stage a été de rajouter un module pour les offres d'emploi,
ainsi les RH peuvent directement éditer les offres depuis un site web, ces offres sont récupérées
par une Web API et affichées pour que les agents puissent postuler facilement (C1).

J'ai aussi eu l'occasion de travailler sur l'optimisation de l'envoi des mails de newsletters
aux 1500 agents, réduisant drastiquement le temps d'envoi et l'utilisation des ressources (C2).
